import 'package:ps_list/models/item_model.dart';
import 'package:ps_list/models/list_model.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  static DatabaseHelper _databaseHelper; // Singleton DatabaseHelper
  static Database _database; // Singleton Database

  String listsTable = 'lists';
  String itemsTable = 'items';

  //Common
  String colId = 'id';
  String colName = 'name';
  String colState = 'state';

  //Table-specific for items
  String colAmount = 'amount';
  String colListId = 'list_id';

  DatabaseHelper._createInstance(); // Named constructor to create instance of DatabaseHelper

  factory DatabaseHelper() {
    if (_databaseHelper == null) {
      _databaseHelper = DatabaseHelper
          ._createInstance(); // This is executed only once, singleton object
    }
    return _databaseHelper;
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await initializeDatabase();
    }
    return _database;
  }

  Future<Database> initializeDatabase() async {
    // Get the directory path for both Android and iOS to store database.
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'ps_list_db.db';

    // Open/create the database at a given path
    var locationsDatabase =
        await openDatabase(path, version: 1, onCreate: _createDb);
    return locationsDatabase;
  }

  void _createDb(Database db, int newVersion) async {
    await db.execute(
        'CREATE TABLE $listsTable($colId INTEGER PRIMARY KEY AUTOINCREMENT, $colName TEXT, $colState INTEGER)');
    await db.execute(
        'CREATE TABLE $itemsTable($colId INTEGER PRIMARY KEY AUTOINCREMENT, $colName TEXT, $colState INTEGER, $colAmount TEXT, $colListId INTEGER, FOREIGN KEY($colListId) REFERENCES $listsTable($colId) ON DELETE CASCADE)');
  }

  // Fetch Operation: Get all objects from database
  /*Future<List<Map<String, dynamic>>> getTodoMapList() async {
    Database db = await this.database;

//		var result = await db.rawQuery('SELECT * FROM $locationsTable order by $colTitle ASC');
    var result = await db.query(locationsTable, orderBy: '$colTitle ASC');
    return result;
  }*/

  // Insert Operation: Insert a object to database
  Future<int> insertList(ListModel listModel) async {
    Database db = await this.database;
    var result = await db.insert(listsTable, listModel.toMap());
    return result;
  }

  Future<int> insertItem(ItemModel itemModel) async {
    Database db = await this.database;
    var result = await db.insert(itemsTable, itemModel.toMap());
    return result;
  }

  //Delete entire lists table
  //TODO: debug only, remove
  Future<int> deleteAllLists() async {
    Database db = await this.database;
    var result = await db.rawDelete('DELETE FROM $listsTable');
    return result;
  }

  //Delete entire items table
  //TODO: debug only, remove
  Future<int> deleteAllItems() async {
    Database db = await this.database;
    var result = await db.rawDelete('DELETE FROM $itemsTable');
    return result;
  }

  Future<int> deleteItem(int itemId) async {
    Database db = await this.database;
    var result = await db.delete(itemsTable, where: '$colId = $itemId');
    return result;
  }

  Future<int> deleteList(int listId) async {
    Database db = await this.database;
    var result = await db.delete(listsTable, where: '$colId = $listId');
    return result;
  }

  Future<int> updateItem(ItemModel itemModel) async {
    Database db = await this.database;
    var result = await db.update(itemsTable, itemModel.toMap(),
        where: '$colId = ${itemModel.id}');
    return result;
  }

  Future<int> updateList(ListModel listModel) async {
    Database db = await this.database;
    var result = await db.update(listsTable, listModel.toMap(),
        where: '$colId = ${listModel.id}');
    return result;
  }

  // Update Operation: Update a object and save it to database
  /*Future<int> updateTodo(Todo todo) async {
    var db = await this.database;
    var result = await db.update(
        locationsTable, todo.toMap(), where: '$colId = ?', whereArgs: [todo.id]);
    return result;
  }*/

  // Delete Operation: Delete a object from database
  /*Future<int> deleteTodo(int id) async {
    var db = await this.database;
    int result = await db.rawDelete(
        'DELETE FROM $locationsTable WHERE $colId = $id');
    return result;
  }*/

  // Get number of lists
  Future<int> getListsCount() async {
    Database db = await this.database;
    List<Map<String, dynamic>> x =
        await db.rawQuery('SELECT COUNT (*) from $listsTable');
    int result = Sqflite.firstIntValue(x);
    return result;
  }

  // Get number of items
  Future<int> getItemsCount() async {
    Database db = await this.database;
    List<Map<String, dynamic>> x =
        await db.rawQuery('SELECT COUNT (*) from $itemsTable');
    int result = Sqflite.firstIntValue(x);
    return result;
  }

  //Get all lists
  Future<List<Map>> getLists() async {
    Database db = await this.database;

    List<Map> result = await db.query(listsTable);

    /*List<Map<String, dynamic>> x =
        await db.rawQuery('SELECT FROM $locationsTable');*/
    return result;
  }

  Future<List<Map>> getItems(int listId) async {
    Database db = await this.database;

    List<Map> result =
        await db.query(itemsTable, where: '$colListId = $listId');

    /*List<Map<String, dynamic>> x =
        await db.rawQuery('SELECT FROM $locationsTable');*/
    return result;
  }
}
