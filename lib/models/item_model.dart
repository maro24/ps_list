class ItemModel {
  int id;
  String name;
  //0 - neutral; 1 - missed; 2 - found
  int state;
  String amount;
  int listId;

  ItemModel(this.name, this.state, this.amount, this.listId);
  ItemModel.withId(this.id, this.name, this.state, this.amount, this.listId);

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['name'] = name;
    map['state'] = state;
    map['amount'] = amount;
    map['list_id'] = listId;

    return map;
  }

  Map<String, dynamic> toMapWithId() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['name'] = name;
    map['state'] = state;
    map['amount'] = amount;
    map['list_id'] = listId;

    return map;
  }
}