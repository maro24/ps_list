class ListModel {
  int id;
  String name;
  //0 - not started; 1 - finished
  int state;

  ListModel(this.name, this.state);
  ListModel.withId(this.id, this.name, this.state);

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['name'] = name;
    map['state'] = state;

    return map;
  }

  Map<String, dynamic> toMapWithId() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['name'] = name;
    map['state'] = state;

    return map;
  }
}