import 'package:flutter/material.dart';
import 'package:ps_list/models/item_model.dart';
import 'package:ps_list/models/list_model.dart';
import 'package:ps_list/utils/db_helper.dart';

class ItemsPage extends StatefulWidget {
  final ListModel list;

  ItemsPage(this.list);

  @override
  State<StatefulWidget> createState() {
    return ItemsPageState();
  }
}

class ItemsPageState extends State<ItemsPage> {
  DatabaseHelper dbHelper = DatabaseHelper();
  var itemsData;

  @override
  void initState() {
    super.initState();

    loadDataFromDb();
  }

  void loadDataFromDb() async {
    var tempListsData = await dbHelper.getItems(widget.list.id);
    setState(() {
      itemsData = tempListsData;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            widget.list.name,
            style: TextStyle(color: Colors.white),
          ),
          brightness: Brightness.dark,
          iconTheme: new IconThemeData(color: Colors.white),
        ),
        body: itemsData == null
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView(children: buildItems()),
        floatingActionButton: FloatingActionButton(
          onPressed: addItemDialog,
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
        ));
  }

  void addItemDialog() {
    TextEditingController amountCtrl = new TextEditingController();
    TextEditingController nameCtrl = new TextEditingController();

    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: AlertDialog(
                  title: Text('Přidat novou položku'),
                  content: Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        TextField(
                          autofocus: true,
                          textCapitalization: TextCapitalization.sentences,
                          maxLength: 8,
                          controller: amountCtrl,
                          decoration: InputDecoration(labelText: 'Množství'),
                        ),
                        TextField(
                          textCapitalization: TextCapitalization.sentences,
                          maxLength: 24,
                          controller: nameCtrl,
                          decoration: InputDecoration(labelText: 'Název'),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 12.0),
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(12.0)),
                                side: BorderSide(
                                    color: Colors.green, width: 2.0)),
                            child: Text(
                              'Přidat',
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colors.green,
                            onPressed: () {
                              addItem(new ItemModel(nameCtrl.text, 0,
                                  amountCtrl.text, widget.list.id));
                            },
                          ),
                        )
                      ],
                    ),
                  )));
        });
  }

  void addItem(ItemModel item) async {
    await dbHelper.insertItem(item);
    Navigator.of(context).pop();
    loadDataFromDb();
  }

  List<Widget> buildItems() {
    List<Widget> children = List<Widget>();
    for (final item in itemsData) {
      children.add(buildItem(ItemModel.withId(item['id'], item['name'],
          item['state'], item['amount'], item['list_id'])));
    }
    return children;
  }

  Container buildItem(ItemModel item) {
    return Container(
      margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(12.0)),
        color: getColorByState(item.state),
        boxShadow: [
          BoxShadow(
            color: Colors.grey[500],
            blurRadius: 4.0, // has the effect of softening the shadow
            spreadRadius: 1.0, // has the effect of extending the shadow
          )
        ],
      ),
      child: ListTile(
        onLongPress: () {
          deleteItem(item.id);
        },
        title: Text(
          item.name,
          style: TextStyle(fontSize: 17.0),
        ),
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              item.amount,
              style: TextStyle(fontSize: 18.0),
            ),
          ],
        ),
        trailing: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              VerticalDivider(
                thickness: 2.0,
              ),
              Container(
                  child: IconButton(
                icon: Icon(Icons.clear),
                onPressed: () {
                  markItemAsMissed(item);
                },
                color: Colors.red,
              )),
              Container(
                  child: IconButton(
                icon: Icon(Icons.check),
                onPressed: () {
                  markItemAsFound(item);
                },
                color: Colors.green,
              ))
            ]),
      ),
    );
  }

  Color getColorByState(int state) {
    switch (state) {
      case 0:
        return Colors.grey[100];
        break;
      case 1:
        return Colors.red[100];
        break;
      case 2:
        return Colors.green[100];
        break;
      default:
        return Colors.grey[100];
        break;
    }
  }

  void markItemAsFound(ItemModel itemModel) async {
    itemModel.state = 2;
    await dbHelper.updateItem(itemModel);
    loadDataFromDb();
  }

  void markItemAsMissed(ItemModel itemModel) async {
    itemModel.state = 1;
    await dbHelper.updateItem(itemModel);
    loadDataFromDb();
  }

  void deleteItem(int itemId) async {
    await dbHelper.deleteItem(itemId);
    loadDataFromDb();
  }
}
