import 'package:flutter/material.dart';
import 'package:ps_list/utils/db_helper.dart';
import 'package:ps_list/models/list_model.dart';

class ListEditorPage extends StatefulWidget {
  final ListModel list;

  ListEditorPage(this.list);

  @override
  State<StatefulWidget> createState() {
    return ListEditorPageState();
  }
}

class ListEditorPageState extends State<ListEditorPage> {
  DatabaseHelper dbHelper = DatabaseHelper();
  TextEditingController nameController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Editor',
            style: TextStyle(color: Colors.white),
          ),
          brightness: Brightness.dark,
          iconTheme: new IconThemeData(color: Colors.white),
        ),
        body: Padding(
              padding: EdgeInsets.all(12.0),
              child: Column(
                children: <Widget>[
                  TextField(
                    textCapitalization: TextCapitalization.sentences,
                    autofocus: true,
                    controller: nameController,
                    decoration: InputDecoration(labelText: 'Name'),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 12.0),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          side: BorderSide(color: Colors.green, width: 2.0)),
                      child: Text(
                        'Vytvořit',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.green,
                      onPressed: () {
                        createList();
                      },
                    ),
                  )
                ],
              ),
            ));
  }

  void createList() async {
    ListModel listModel = new ListModel(nameController.text, 0);
    await dbHelper.insertList(listModel);
    Navigator.pop(context);
  }
}
