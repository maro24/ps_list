import 'dart:convert';
import 'package:ps_list/pages/list_editor.dart';
import 'package:ps_list/pages/items_page.dart';
import 'package:flutter/material.dart';
import 'package:ps_list/models/list_model.dart';
import 'package:ps_list/utils/db_helper.dart';

class ListsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ListsPageState();
  }
}

class ListsPageState extends State<ListsPage> {
  DatabaseHelper dbHelper = DatabaseHelper();
  var listsData;

  @override
  void initState() {
    super.initState();

    loadDataFromDb();
  }

  void loadDataFromDb() async {
    var tempListsData = await dbHelper.getLists();
    setState(() {
      listsData = tempListsData;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Seznamy',
          style: TextStyle(color: Colors.white),
        ),
        brightness: Brightness.dark,
        iconTheme: new IconThemeData(color: Colors.white),
      ),
      body: listsData == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView(children: buildLists()),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          openListEditor(null);
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }

  void openListEditor(ListModel list) async {
    await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => ListEditorPage(list)));
    loadDataFromDb();
  }

  void openList(ListModel list) async {
    await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => ItemsPage(list)));
    loadDataFromDb();
  }

  List<Widget> buildLists() {
    List<Widget> items = List<Widget>();
    for (final list in listsData) {
      items.add(buildListItem(
          new ListModel.withId(list['id'], list['name'], list['state'])));
    }
    return items;
  }

  void deleteList(int listId) async {
    await dbHelper.deleteList(listId);
    loadDataFromDb();
  }

  Container buildListItem(ListModel list) {
    return Container(
        margin: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 0),
        child: Ink(
            decoration: BoxDecoration(
                color: Colors.grey[100],
                borderRadius: BorderRadius.circular(12.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[500],
                    blurRadius: 4.0, // has the effect of softening the shadow
                    spreadRadius: 1.0, // has the effect of extending the shadow
                  )
                ]),
            child: InkWell(
              highlightColor: Colors.amberAccent,
              borderRadius: BorderRadius.circular(12.0),
              onTap: () {
                openList(list);
              },
              splashColor: Colors.amberAccent,
              child: ListTile(
                  title: Text(list.name),
                  leading: list.state == 0
                      ? Icon(
                          Icons.shopping_cart,
                          color: Colors.amber,
                        )
                      : Icon(
                          Icons.check_circle,
                          color: Colors.green,
                        ),
                  trailing: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () {
                          openRenameDialog(list);
                        },
                      ),
                      IconButton(
                        icon: Icon(Icons.delete, color: Colors.red,),
                        onPressed: () {
                          deleteList(list.id);
                        },
                        splashColor: Colors.redAccent.withOpacity(.2),
                      ),
                    ],
                  )),
            )));
  }

  void openRenameDialog(ListModel list) {
    TextEditingController nameCtrl = new TextEditingController();
    nameCtrl.text = list.name;

    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: AlertDialog(
                  title: Text('Přejmenovat seznam'),
                  content: Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        TextField(
                          textCapitalization: TextCapitalization.sentences,
                          autofocus: true,
                          controller: nameCtrl,
                          decoration: InputDecoration(labelText: 'Název'),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 12.0),
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(12.0)),
                                side: BorderSide(
                                    color: Colors.green, width: 2.0)),
                            child: Text(
                              'Uložit',
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colors.green,
                            onPressed: () {
                              list.name = nameCtrl.text;
                              renameList(list);
                            },
                          ),
                        )
                      ],
                    ),
                  )));
        });
  }

  void renameList(ListModel list) async {
    await dbHelper.updateList(list);
    Navigator.of(context).pop();
    loadDataFromDb();
  }
}
